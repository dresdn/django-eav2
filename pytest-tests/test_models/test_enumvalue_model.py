from hypothesis import given
from hypothesis.extra import django

from eav.models import EnumValue


class TestEnumValue(django.TestCase):
    """This is a property-based test that ensures model correctness."""

    @given(django.from_model(EnumValue))
    def test_model_properties(self, instance: EnumValue):
        """Tests that instance can be saved and has correct representation."""
        instance.save()

        assert instance.id > 0
        assert len(str(instance)) <= 100
