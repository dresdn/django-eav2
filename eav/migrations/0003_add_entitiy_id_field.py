import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('eav', '0002_add_entity_ct_field'),
    ]

    operations = [
        migrations.AddField(
            model_name='attribute',
            name='entity_id',
            field=models.CharField(blank=True, max_length=36),
        ),
        migrations.RemoveField(
            model_name='attribute',
            name='entity_ct',
        ),
        migrations.AddField(
            model_name='attribute',
            name='entity_ct',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name='attribute_entities',
                to='contenttypes.contenttype',
            ),
        ),
        migrations.AlterField(
            model_name='enumvalue',
            name='value',
            field=models.CharField(
                db_index=True, max_length=100, verbose_name='Value'
            ),
        ),
        migrations.AlterField(
            model_name='value',
            name='entity_id',
            field=models.CharField(blank=True, max_length=36),
        ),
    ]
